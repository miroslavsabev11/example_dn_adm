from setuptools import setup, find_packages

setup(name='DownloaderNext and AdMarker',
      version=1.1,
      description='DownloaderNext and AdMarker automated sanity test cases',
      author='ExampleE',
      author_email='example@e.com',
      packages=find_packages()
      )
