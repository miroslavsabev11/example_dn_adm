from behave import *
from CommonFunc import credentialsHelper, webcommon, DB_helpers
from CommonConfigs import locators, urlconfig, env
import time, sys, boto3, requests, json, logging
from CommonLogger import logger

# initiate the logger
logger.logger()

"""Functions in here respond to the last scenario from AdMarker.feature 
(Scenario: Compare Database AdMarker rule counts)"""


@given('the "{db_env}" Database is connected')
def AdMarker_db_connection(context, db_env):
    context.db_env = db_env
    context.DB_class = DB_helpers.DBHelper(db_env)
    logging.info('AdMarker datebase selected:', db_env)
    if db_env == "AdMarker-ST":
        record = context.DB_class.execute_select(env.DB_sqls['adMarker_count_ST'])
    else:
        record = context.DB_class.execute_select(env.DB_sqls['adMarker_count_for_other_envs'])
    context.records = record.copy()


@given('the count SQL query executed')
def count_sql_query_output(context):
    logging.info('Fetching the rules count from the Database results..')
    logging.info(f'Number of AdMarker rules in {context.db_env}:', context.records['count(*)'])
    context.date_last_message_DB = int(context.records['count(*)'])


@when('the result has been recorded')
def results_recorded(context):
    logging.info('Recording the result..')


@then('admarkerrule count should be greater or equal to the number "{result_count}"')
def admrules_greater_or_equal(context, result_count):
    logging.info('Comparing the results..')
    logging.info(f'Checking if {context.db_env} has results equal or greater than {result_count}..')
    if context.date_last_message_DB >= int(result_count):
        logging.info(f'Test Passed! {context.date_last_message_DB} is equal or greater than {result_count}.')
        assert True
    else:
        logging.error(f'Error, AdMarker rule count for {context.db_env} is smaller than {result_count}!')
        assert False
