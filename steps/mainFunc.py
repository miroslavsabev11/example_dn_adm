from behave import *
# from selenium import webdriver
from CommonFunc import credentialsHelper, webcommon, DB_helpers
from CommonConfigs import locatorconfig, urlconfig, env
import time, sys, boto3, requests, json, logging
from datetime import datetime, timezone
from CommonLogger import logger

# initiate the logger
logger.logger()


@step("the HealthCheck for '{environment}' is triggered")
@given('the HealthCheck for "{environment}" is triggered')
def healtheck_trigger(context, environment):
    try:
        if 'DownloaderNext' in environment:
            uri = urlconfig.URLCONFIG.get('DownloaderNext')
            env_prefix = env.get_environment(environment);  # call function get_environment from the env file
            logging.info(f"Running Health Check on {environment}.")
            uri = uri.replace("#", env_prefix)  # replaces '#' with env_prefix for stg, tst, dev and prod
            logging.info(f"The Health Check URL for {environment} is {uri}")
        elif 'AdMarker' in environment:
            uri = urlconfig.URLCONFIG.get('AdMarker_HealthCheck')
            logging.info(f"Running Health Check for {environment}")
            env_prefix = env.get_environment(environment);  # call function get_environment from the env file
            #  adm port prefix will select the proper port for AdMarker Healthcheck
            adm_port_prefix = env.get_adm_port(environment)
            # uri = uri.replace("#", env_prefix)  # replaces '#' with env_prefix for stg, tst, dev and prod
            """"translate() is fucntion that replaces multiple vars insead of using replace() which is for single
            "#" replaces the URL for the specified environment, "?" replaces the port for it"""

            uri = uri.translate(str.maketrans({"#": env_prefix, "?": adm_port_prefix}))
            logging.info(f"The Health Check URL for {environment} is {uri}")
            logging.info("Running Health Check test for ", environment)

        response = requests.get(uri)
        context.status_response = response.status_code
        context.healthcheck_json_data = response.json()  # get the json response from the request
        logging.info(f'GET response JSON body: {context.healthcheck_json_data}')
    except:
        logging.error(f"Status code: {context.status_response}")
        logging.error(f"Error message: {sys.exc_info()}")
        logging.error("Failed to send the GET request!")
        assert False
    time.sleep(2)


@when('the status response is returned')
def response_returned(context):
    logging.info("Returning status response")


@then('the status response will return code 200 OK')
def check_status_response(context):
    if context.status_response == 200:
        logging.info('Status response is 200, Test passed!')
        assert True
    else:
        logging.error("Health Check failed, status code is NOT 200!")
        assert False


@then('the "artifactNumber" field will be "{expectedArtifactNumber}"')
def check_artifactNumber(context, expectedArtifactNumber):
    artifactNumber = context.healthcheck_json_data[
        'artifactNumber']
    string_artifactNumber = str(artifactNumber)

    if string_artifactNumber == expectedArtifactNumber:
        logging.info(f'Articaft value: {string_artifactNumber}')
        assert True
    else:
        logging.info(f'Artifact number does not match the expected value!')
        logging.info(f'Expected Artifact value: {expectedArtifactNumber}')
        logging.info(f'Actual artifactNumber: {string_artifactNumber}')
        assert False


@then('the "versionNumber" field will be "{version}"')
def check_versionNumber(context, version):
    versionNumber = context.healthcheck_json_data['versionNumber']
    string_versionNumber = str(versionNumber)
    if string_versionNumber == version:
        logging.info(f'DownloaderNext version: {version}')
        assert True
    else:
        logging.info(f'Version does not match the expected outcome!')
        logging.info(f'Version expected: {version}')
        logging.info(f'Actual version from Heath Check: {string_versionNumber}')
        assert False


# ------------------------------------------------------------------------------
# Scenario 2, STEP 1
@step("SNS publishes message '{sns_message}' from '{AWS_env}' environment")
@given('SNS publishes message "{sns_message}" from "{AWS_env}" environment')
def go_to_url(context, sns_message, AWS_env):
    logging.info(f'SNS message body used: {sns_message}')
    logging.info(f'AWS environment used: {AWS_env}')
    try:
        env_prefix = env.get_sns_env(AWS_env)
        access_key = env.get_sns_env(AWS_env).get('Access_key')
        secret_key = env.get_sns_env(AWS_env).get('Secret_key')
        topicArn = env.get_sns_env(AWS_env).get('topicArn')
        snsClient = boto3.client(
            'sns',
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
            region_name=env.get_sns_env(AWS_env).get('region')
        )
        # MESSAGE NUMBER TO PUBLISH
        publishOject = env.get_public_message(sns_message)
        logging.info(f'Message body: {publishOject}')
        response = snsClient.publish(TopicArn=topicArn,
                                     Message=json.dumps(publishOject),
                                     Subject='AutoTest DEV-QA')
        context.status_response = response['ResponseMetadata']['HTTPStatusCode']
        logging.info('SNS message send..')
        logging.info(f'Status response: {context.status_response}')

        # datetime object containing current date and time
        context.now = datetime.now(tz=timezone.utc)
        date_of_message_send = context.now.strftime("%Y-%m-%d %H:%M")
        context.string_date_of_message_send = str(date_of_message_send)
    except:
        logging.error(f'Sending message via {AWS_env} is failing')
        logging.error(f'Status response: {context.status_response}')
        assert False


@when('the "{db_env}" Database workflow table is opened')
def check_DB(context, db_env):

    context.db_env = db_env
    time.sleep(4)
    context.DB_class = DB_helpers.DBHelper(db_env)

    record = context.DB_class.execute_select(env.DB_sqls['workflow_latest_row'])
    context.records = record.copy()
    logging.info(f'Fetching the last record from the {db_env} Database.')
    logging.info(f"Most recent result is from: {context.records['created']}")
    date_last_message_DB = str(context.records['created'])
    logging.info(f"Time of latest message published by SNS: {context.string_date_of_message_send}")
    if context.string_date_of_message_send in date_last_message_DB:
        logging.info(f'Date being validated against: {date_last_message_DB}')
        assert True
    else:
        logging.error('Job Failed!')
        logging.error('No new entry detected in the Database.')
        logging.error('Dates do not match! Message timestamps do not match.')
        assert False


@when('new JOB with conversation id "{conv_id}" has been created in the table')
def job_has_been_created_in_table(context, conv_id):
    logging.info('Checking if Conversation ID in the Database matches the assigned one in the .feature file')
    if int(context.records['identifier']) == int(conv_id):
        logging.info(f"Conversation IDs match!")
        logging.info(f'Conversation ID identifier: {conv_id}')
        assert True
    else:
        logging.error('Conversation IDs do not match or the job failed!')
        assert False


@when('some time is given for the system to process the request')
def time_given(context):
    logging.info('System is processing the request...')
    time.sleep(4)
    logging.info('Accessing the Database.')
    # this condition might be deprecated at later stage or new functionality added on its place


@then('the new job will go into "{state_status}" state')
def job_state(context, state_status):
    for i in range(20):
        context.DB_class = DB_helpers.DBHelper(context.db_env)
        record = context.DB_class.execute_select(env.DB_sqls['workflow_latest_row'])
        records = record.copy()
        logging.info("State status: ", str(records['state']))
        if str(records['state']) == state_status:
            logging.info("Job Completed!")
            assert True
            break
        elif str(records['state']) == 'FAILED':
            logging.error(f"The job failed with the following message: ")
            logging.error(records['error_message'])
            logging.error('Job Failed!')
            assert False
            break
        else:
            logging.info("Job is being processed, checking the status again in 10 seconds")
            time.sleep(10)


# From AdMarker extra steps
@then('buildNumber is "{version}"')
def buildNumber_AdMarker(context, version):
    logging.info(f'User buildNumber input: {version}')
    logging.info(f"Checking AdMarker's current version from the Health Check..")
    logging.info(f"Version from Health Check body response: {context.healthcheck_json_data['buildNumber']}")
    logging.info("Comparing versions..")
    if version in context.healthcheck_json_data['buildNumber']:
        logging.info(f'Build number versions match!')
        assert True
    else:
        logging.error(
            f'Versions do not match! HealthCheck response version is {context.healthcheck_json_data["buildNumber"]}')
        logging.error(f'User expected version is {version}')
        assert False


@given('the "{request}" POST request is triggered for "{environment}"')
def adjustAdMarker_request(context, request, environment):
    uri = urlconfig.URLCONFIG.get('AdMarker')
    env_prefix = env.get_environment(environment);  # call function get_environment from the env file
    logging.info(f"Running Health Check test on {environment}.")
    adm_port_prefix = env.get_adm_port(environment)
    uri = uri.translate(str.maketrans(
        {"#": env_prefix, "?": adm_port_prefix}))  # replaces '#' with env_prefix for stg, tst, dev and prod
    logging.info(f"The Health Check URL for {environment} is {uri}")
    if request == 'adRules':
        headers = {'content-type': 'application/xml'}
        body = env.get_AdMarker_messages('adRules')
        add_up = '/admarkersvc-war/ws/rest/adMarkerService/adMarkerRule?serviceCode=VM&providerCode=BSKY_SKY1_HD&termType=Archive&studioName=Fox' + ' ' + 'Broadcasting&aspectRatio=16x9&txProfile=VM-Sky_IPVOD_HD_5.1EN_16x9'
        link = uri + add_up
        logging.info(f"Sending POST request to: {link}")
        response = requests.post(link, data=body, headers=headers)
        context.status_response = response.status_code
        logging.info(f'Fetching status response for the {request}..')
        logging.info(f'Status response: {context.status_response}')
    elif request == 'adjustAdMarker':
        headers = {'content-type': 'application/json'}
        body = env.get_AdMarker_messages('adjustAdMarker')
        link = uri + '/admarkersvc-war/ws/rest/adMarkerService/adjustAdMarker?serviceCode=NSV'
        logging.info(f"Sending POST request to: {link}")
        response = requests.post(link, data=json.dumps(body), headers=headers)
        context.status_response = response.status_code
        context.adjustAdMarker_json_data = response.json()  # get the json response from the request
        logging.info(f'Fetching status response for the {request}..')
        logging.info(f'Status response: {context.status_response}')


@then('return expected body message')
def return_expected_body_message(context):
    expected_body = {
        "adMarker": "10.0,803.44,1527.44,1863.16,2513.92",
        "runTime": 0.000
    }
    if expected_body == context.adjustAdMarker_json_data:
        logging.info('The return message match!')
        assert True
    else:
        logging.error('Return message does not match to expected body!')
        assert False
