Feature: Automated Sanity tests for AdMarker-PROD
#  healthcheck available for AdMarker-ST/SIT/UAT/PROD
  @HC_ADM
  Scenario: Checking if AdMarker is functioning
    Given the HealthCheck for "AdMarker-PROD" is triggered
    When the status response is returned
    Then the status response will return code 200 OK
    And buildNumber is "ver:1.10.0"

  Scenario: Checking if adjustAdMarker is functioning
    Given the "adjustAdMarker" POST request is triggered for "AdMarker-PROD"
    When the status response is returned
    Then the status response will return code 200 OK
    And return expected body message

  Scenario: Checking if adRules is functioning
    Given the "adRules" POST request is triggered for "AdMarker-PROD"
    When the status response is returned
    Then the status response will return code 200 OK

  Scenario: Check AdMarker database rule counts
    Given the "AdMarker-PROD" Database is connected
    And the count SQL query executed
    When the result has been recorded
    Then admarkerrule count should be greater or equal to the number "8261"