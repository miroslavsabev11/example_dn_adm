Feature: Automated Sanity tests for DownloaderNext-TST

#  healthcheck available for DownloaderNext-DEV/SIT/STG/TST/PRD

  @DN_HC
  Scenario: Checking if DownloaderNext-TST is functioning
    Given the HealthCheck for "DownloaderNext-TST" is triggered
    When the status response is returned
    Then the status response will return code 200 OK
    And the "artifactNumber" field will NOT be null value
    And the "versionNumber" field will NOT be null value

  @DB
  Scenario: Input to DownloaderNext via SNS queue and validate DB tables
    Given SNS publishes message "dev_message_2" from "AWS_dev" environment
    When the "DN-DEV" Database workflow table is opened
    And new JOB with conversation id "33444555" has been created in the table
    And some time is given for the system to process the request
    Then the new job will go into "COMPLETED" state