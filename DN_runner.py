import logging, requests, argparse, os, time
from CommonLogger import logger

''''
Runner for DownloaderNext
Command example:
python DN_runner.py -e DownloaderNext-PROD -hc DN_HC
python DN_runner.py -e DownloaderNext-TST -m dev_message_2 -a AWS_dev -db DN-DEV -id 33444555
'''


def main():
    ''''
            :param env is DownloaderNext-DEV/SIT/STG/TST/PRD
            :param dev_message is DownloaderNext environment, available: DownloaderNext-DEV/SIT/STG/TST/PRD
            :param aws_environment is SNS message to be triggered, example: dev_message_1/2
            :param dn_DB is the Workbench Database to be used, example: DN-DEV/DN-TST
            :param _conv_id is the conversation id for the selected message: 55419/33444555
            '''
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-e', '--environment',
                            required=True,
                            help="Select DownloaderNext environment, available: DownloaderNext-DEV/SIT/STG/TST/PRD.")
        parser.add_argument('-m', '--message',
                            required=False,
                            help='Select SNS message to be triggered, example: dev_message_1/2.')
        parser.add_argument('-a', '--aws_environment',
                            required=False,
                            help='Select AWS environment from which to trigger the message, '
                                 'example: AWS_dev/AWS_tst/AWS_stg/AWS_sit.')
        parser.add_argument('-db', '--DN_Database',
                            required=False,
                            help='Workbench Database to be used, example: DN-DEV/DN-TST.')
        parser.add_argument('-id', '--conv_id',
                            required=False,
                            help='Conversation id for the selected message: 55419/33444555. '
                                 'First message goes with the first conv_id, second one with the second.')
        parser.add_argument('-hc', '--health_check',
                            required=False,
                            help='Run only Scenarios with the "HC" tag for the HealthCheck.')

        args = parser.parse_args()
        env = args.environment
        message = args.message
        aws_environment = args.aws_environment
        DN_Database = args.DN_Database
        conv_id = args.conv_id

        # need to declare it as global so it can go into the runner() function
        global health_check
        health_check = args.health_check

        with open("P:/Users/Example/features/Gherkin/DownloaderNext_Template.feature") as r:
            text = r.read().replace("DownloaderNext_environment", str(env))
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature", "w") as w:
            w.write(text)
        # 2
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature") as r:
            text_1 = r.read().replace("dev_message", str(message))
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature", "w") as w:
            w.write(text_1)
        # 3
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature") as r:
            text_1 = r.read().replace("aws_topic", str(aws_environment))
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature", "w") as w:
            w.write(text_1)
        # 4
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature") as r:
            text_1 = r.read().replace("dn_DB", str(DN_Database))
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature", "w") as w:
            w.write(text_1)
        # 5
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature") as r:
            text_1 = r.read().replace("_conv_id", str(conv_id))
        with open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature", "w") as w:
            w.write(text_1)
        # check if file exists
        try:
            f = open("P:/Users/Example/DownloaderNext/features/DownloaderNext_Template_Runner.feature")
        except IOError:
            print("File not accessible")
        finally:
            f.close()
    except FileNotFoundError as f:
        logging.error("Could not find the file!")


def runner():
    try:
        if health_check is None:
            os.system('behave DownloaderNext_Template_Runner.feature -f json -o my_json_out.json')
        if str(health_check) == 'DN_HC':
            os.system(
                'behave DownloaderNext_Template_Runner.feature --tags=DN_HC -f json -o my_json_out.json --no-skipped')
        else:
            logging.error('Error in Health Check argument, check if your input is valid (for example DN_HC)')
    except TypeError as e:
        logging.error('Test cases failed, check logs!')


if __name__ == '__main__':
    logger.logger()
    main()
    runner()
