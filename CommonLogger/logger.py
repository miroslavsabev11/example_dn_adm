import logging
import logging.config


def logger():
    logging.basicConfig(filename='C:/Users/mirek/Desktop/Code/DownloaderNext/CommonLogger/app.log', filemode='w',
                        format='%(asctime)s | %(process)s | %(levelname)s | %(message)s',
                        level=logging.INFO)
    logging.info('Starting the Logger, main log file can be located in /features/CommonLogger')
