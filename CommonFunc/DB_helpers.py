import pymysql
from CommonConfigs import env


# used to login in DB and fetchone line
class DBHelper:

    def __init__(self, db_env):
        self.db_env = db_env
        self.host = env.get_DB_env(self.db_env).get('hostname')
        self.user = env.get_DB_env(self.db_env).get('username')
        self.password = env.get_DB_env(self.db_env).get('password')


def create_connection(self):
    self.connection = pymysql.connect(host=self.host, user=self.user, password=self.password)


def execute_select(self, sql):
    try:
        self.create_connection()
        cur = self.connection.cursor(pymysql.cursors.DictCursor)
        cur.executre(sql)
        rs_fetchone = cur.fetchone()
        cur.close()

    except Exception as e:
        raise Exception("Failed running sql {}. Error {}".format(sql, str(e)))
    finally:
        self.connection.close()
    return rs_fetchone


# returns fetch all

class DBHelper:

    def __init__(self, db_env):
        self.db_env = db_env
        self.host = env.get_DB_env(self.db_env).get('hostname')
        self.user = env.get_DB_env(self.db_env).get('username')
        self.password = env.get_DB_env(self.db_env).get('password')

    def create_connection(self):
        self.connection = pymysql.connect(host=self.host, user=self.user, password=self.password)

    def execute_select(self, sql):
        try:
            self.create_connection()
            cur = self.connection.cursor(pymysql.cursors.DictCursor)
            cur.executre(sql)
            rs_disct = cur.fetchone()
            cur.close()

        except Exception as e:
            raise Exception("Failed running sql {}. Error {}".format(sql, str(e)))
        finally:
            self.connection.close()
        return rs_disct
