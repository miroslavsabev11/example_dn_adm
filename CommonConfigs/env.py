"""
Common environment functions
"""

import lxml.etree as ET


def get_AdMarker_messages(argument):
    switcher = {
        "adRules": xml_parser(),
        "adjustAdMarker": {
            "adMarker": "9.96,803.44,1527"
        }
    }
    return switcher.get(argument, "nothing")


def xml_parser():
    xmlObject = ET.parse("Path to the adRules.xml")
    pretty_xml_as_string = ET.tostring(xmlObject).decode()

    return pretty_xml_as_string


def get_environment(argument):
    switcher = {
        "NextDownloader-DEV": "dev",
        "NextDownloader-SIT": "sit",
        "NextDownloader-PRD": "prd",
        "NextDownloader-TST": "tst",
        "AdMarker-ST": "111",
        "AdMarker-SIT": "88",
        "AdMarker-PROD": "00"
    }
    return switcher.get(argument, "nothing")


def get_sns_env(argument):
    switcher = {
        "AWS_dev": AWS_dev,
        "AWS_tst": AWS_tst
    }

    return switcher.get(argument, "nothing")


def get_DB_env(argument):
    switchet = {
        "ND-DEV": AWS_dev,
        "ND-TST": AWS_tst,
        "AdMarker-PROD": ADM_prod,
        "AdMarker-UAT": ADM_uat,
        "AdMarker-SIT": ADM_sit,
        "AdMarker-ST": ADM_st
    }


AWS_dev = {
    'Access_key': 'INSERT ACCESS KEY HERE',
    'Secret_key': 'INSERT SECRET KEY HERE',
    'topicArn': 'INSERT TOPICARN HERE',
    'region': 'INSERT REGION HERE',
    'hostname': 'INSERT HOSTNAME HERE',
    'username': 'INSERT USERNAME HERE',
    'password': 'INSERT PASSWORD HERE',
}

AWS_tst = {
    'Access_key': 'INSERT ACCESS KEY HERE',
    'Secret_key': 'INSERT SECRET KEY HERE',
    'topicArn': 'INSERT TOPICARN HERE',
    'region': 'INSERT REGION HERE',
    'hostname': 'INSERT HOSTNAME HERE',
    'username': 'INSERT USERNAME HERE',
    'password': 'INSERT PASSWORD HERE',
}

ADM_prod = {
    'hostname': 'enter hostname',
    'username': '',
    'password': ''
}

ADM_tst = {
    'hostname': 'enter hostname',
    'username': '',
    'password': ''
}

ADM_uat = {
    'hostname': 'enter hostname',
    'username': '',
    'password': ''
}

ADM_st = {
    'hostname': 'enter hostname',
    'username': '',
    'password': ''
}

ADM_sit = {
    'hostname': 'enter hostname',
    'username': '',
    'password': ''
}


def get_public_message(argument):
    dev_message_1_vam_id = '222222'

    dev_message_1_conv_id = '11111'

    switchet = {
        "dev_message_1"" {}"
    }

    return switchet.get(argument, "")


DB_sqls = {
    'db_sql_example': "SELECT * FROM example"
}
