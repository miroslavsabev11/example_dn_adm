import logging, requests, argparse, os, time
from CommonLogger import logger

''''
Runner for AdMarker
Command example:
python AdMarker_runner.py -e AdMarker-PROD -v ver:1.10.0
'''


def main():
    ''''
            :param env is AdMarker-ST/SIT/UAT/PROD
            :param version is in the format ver:1.10.0 for example
            '''
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-e', '--environment',
                            required=True,
                            help="Select AdMarker environment, available: AdMarker-ST/SIT/UAT/PROD")
        parser.add_argument('-v', '--version',
                            required=True,
                            help='Select version to check for AdMarker, for example: -v ver:1.10.0')
        # parses/stores all args in this var
        args = parser.parse_args()

        env = args.environment
        version = args.version

        # locate Template file and replace vars with the ones from the argument parser
        with open("P:/Users/MSabev/Desktop/DownloaderNext/features/Gherkin/AdMarker_Template.feature") as r:
            text = r.read().replace("env", str(env))
        with open("P:/Users/MSabev/Desktop/DownloaderNext/features/AdMarker_Template_Runner.feature", "w") as w:
            w.write(text)
        with open("P:/Users/MSabev/Desktop/DownloaderNext/features/AdMarker_Template_Runner.feature") as r:
            text_1 = r.read().replace("version", str(version))
        with open("P:/Users/MSabev/Desktop/DownloaderNext/features/AdMarker_Template_Runner.feature", "w") as w:
            w.write(text_1)
        # check if file exists
        try:
            f = open("P:/Users/MSabev/Desktop/DownloaderNext/features/AdMarker_Template_Runner.feature")
        except IOError:
            print("File not accessible")
        finally:
            f.close()

    except FileNotFoundError as f:
        logging.error("Could not find the file!")


def runner():
    try:
        os.system('behave AdMarker_Template_Runner.feature -f json -o my_json_out.json')
    except TypeError as e:
        print('Test cases failed, check logs!')


if __name__ == '__main__':
    logger.logger()
    main()
    runner()
